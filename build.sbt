name := "yandex-dictionary-impl"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.1"

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)



val akkaVer = "2.4.17"
val akkaHttpVer = "10.0.4"


libraryDependencies ++= Seq(
  "com.typesafe.akka"       %% "akka-actor"                 % akkaVer,
  "com.typesafe.akka"       %% "akka-stream"                % akkaVer,
  "com.typesafe.akka"       %% "akka-testkit"               % akkaVer,
  "com.typesafe.akka"       %% "akka-http"                  % akkaHttpVer,
  "com.github.fomkin"       %% "pushka-json"                % "0.8.0",
  "org.scalatest"           %% "scalatest"                  % "3.0.1"               % "test"
)

enablePlugins(LocPlugin)

