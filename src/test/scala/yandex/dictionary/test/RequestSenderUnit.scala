package yandex.dictionary.test

import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpEntity
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}

import yandex.dictionary.DictionarySettings
import yandex.dictionary.DictionarySettings._
import yandex.dictionary.client.Sender
import yandex.dictionary.model._

class RequestSenderUnit extends ActorTestSpec {

  implicit val materializer = ActorMaterializer()

  val params = TranslationRequest("en-ru", "time")
  val wrongKey = "dict.1.1.20151225T153210Z.46a2df6e415947b7.e78b4ba3f056556482cfa4565128a6d8e34186cf"
  val wrongParams = TranslationRequest("ru-ds", "hello")
  val partialUri = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?"
  val uri = partialUri + s"key=${DictionarySettings.dictKey}&lang=${params.lang}&text=${params.text}"
  val wrongUri = partialUri + s"key=wrong&lang=${wrongParams.lang}&text=${wrongParams.text}"


  override def afterAll {
    Http(system).shutdownAllConnectionPools() andThen { case _ => shutdown() }
  }

  "An RequestSender Actor" must {
    "receive HttpEntity" in {
      val reqRef = system.actorOf(Props(classOf[MockController], uri, testActor,"sender-1"), "mock-1")
      expectMsg("success answer")
    }
  }

  "An RequestSender Actor" must {
    "receive ErrKeyInvalid" in {
      val reqRef = system.actorOf(Props(classOf[MockController], wrongUri, testActor,"sender-2"), "mock-2")
      expectMsg("error")
    }
  }
}

class MockController(req: TranslationRequest, receiver: ActorRef, name: String) extends Actor {

  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  val reqRef = context.actorOf(Sender.props(req), name)

  override def receive: Receive = {
    case x: Answer[_] if x.code == 200 =>
      receiver ! "success answer"
    case Answers.Forbidden => receiver ! "error"
  }
}

