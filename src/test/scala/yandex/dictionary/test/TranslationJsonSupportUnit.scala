package yandex.dictionary.test

import pushka.json._
import yandex.dictionary.model._

class TranslationJsonSupportUnit extends CommonTestSpec {
  val emptyTranslatedWord = TranslatedWord(Set())
  val emptyTranslatedWordJson = write(emptyTranslatedWord)
  val translation = TranslatedWord(
    Set(TranslatedPair(text = "go away", Some("verb"), ts = Some("g?? ??we?"), Set(
      Translation("уходить", Some("verb"),
        Some(List(
          Synonym("уезжать", Some("verb"), None),
          Synonym("уйти", Some("verb"), None),
          Synonym("уехать", Some("verb"), None),
          Synonym("удалиться", Some("verb"), None),
          Synonym("убраться", Some("verb"), None))),
        Some(List(
          SingleText("leave"),
          SingleText("go"), SingleText("retire"), SingleText("clean"))), None),
      Translation("пойти прочь", Some("verb"),
        Some(List(
          Synonym("идти прочь", Some("verb"), None))),
        Some(List(SingleText("walk away"))), None),
      Translation("исчезнуть", Some("verb"),
        Some(List(Synonym("исчезать", Some("verb"), None))),
        Some(List(SingleText("disappear"))), None),
      Translation("разойтись", Some("verb"), None,
        Some(List(SingleText("disperse"))), None)))))

  val translationJson = write(translation)

  "An empty `def` in translation json" should "be Set()" in {
    read[TranslatedWord](emptyTranslatedWordJson) shouldBe emptyTranslatedWord
  }

  "An full `def` in translation" should "be filled" in {
    read[TranslatedWord](translationJson) shouldBe translation
  }
}
