package yandex.dictionary.test

import akka.actor.ActorSystem
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

abstract class ActorTestSpec
  extends TestKit(ActorSystem("TestKitUsageSpec"))
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll
    with DefaultTimeout
    with ImplicitSender
