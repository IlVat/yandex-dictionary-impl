package yandex.dictionary

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global

import yandex.dictionary.DictionarySettings._
import yandex.dictionary.common.Common
import yandex.dictionary.routes.YandexDictionaryApi

object Boot extends App with Common {
  import RequestTimeout._

  override implicit val system = ActorSystem(sys)
  override implicit val materializer = ActorMaterializer()

  val routes = new YandexDictionaryApi(system, timeout).route
  val server = Http().bindAndHandle(routes, interface = interface, port = port)

  server.foreach {
    _ => println(s"Server successfully bound to $interface: $port")
  }

}


object RequestTimeout {
  implicit def asFiniteDuration(d: java.time.Duration): Timeout =
    Timeout(scala.concurrent.duration.Duration.fromNanos(d.toNanos))

}
