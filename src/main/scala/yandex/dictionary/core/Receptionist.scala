package yandex.dictionary.core

import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}

import yandex.dictionary.model.{TranslationRequest, Translations}

class Receptionist extends Actor with ActorLogging {

  implicit val mat: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
  val cacheRef = context.actorOf(Props[Cache], "cache")
  var senderRef = Actor.noSender
  var reqNo = 0

  def receive: Receive = {
    case req @ TranslationRequest(_, _) =>
      log.info(s"Request ${req.text} was received.")
      senderRef = sender
      val controller = context.actorOf(Controller.props(cacheRef))
      controller ! req
    case tr: Translations =>
      log.info(s"${self.path.name} received translation")
      senderRef ! tr
  }
}
