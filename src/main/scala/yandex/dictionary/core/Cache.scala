package yandex.dictionary.core

import akka.actor.{Actor, ActorLogging}

import yandex.dictionary.core.Controller.{Check, Checked}
import yandex.dictionary.model.TranslatedWord

class Cache extends Actor with ActorLogging {
  var cache = Map.empty[String, TranslatedWord]

  override def receive = {
    case Check(words) =>
      val coincidence = words intersect cache.keySet
      sender ! Checked(cache.filterKeys(coincidence.contains).values.toSet)
      log.info(s"$words were checked in cache")
    case value @ TranslatedWord(tr) if tr.nonEmpty =>
      val word = value.`def`.toVector.head.text
      cache += (word -> value)
      log.info(s"$word was added to cache")
    case x => log.info(s"$x undefined event in cache")
  }
}
