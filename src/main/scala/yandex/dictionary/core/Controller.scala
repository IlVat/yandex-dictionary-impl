package yandex.dictionary.core

import scala.language.postfixOps
import scala.concurrent.duration._

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.stream.ActorMaterializer

import yandex.dictionary.client.Sender
import yandex.dictionary.core.Controller._
import yandex.dictionary.helpers.TextHandler
import yandex.dictionary.model._

class Controller(cache: ActorRef)(
    implicit materializer: ActorMaterializer) extends Actor with ActorLogging {
  //TODO must cache translated words
  //firstly in memory, than e.g. in redis
  var words = Set.empty[String]
  var senders = Set.empty[ActorRef]
  var lg = ""
  var translations = Set.empty[TranslatedWord]

  def receive: Receive = checking

  def checking: Receive = {
    case TranslationRequest(lang, text) =>
      lg = lang
      words = TextHandler.handle(text)
      cache ! Check(words)
      log.info(s"Check cache for $words")
    case Checked(tr) =>
      log.info(s"$tr was received from cache")
      println(words)
      val checked = tr.flatMap(_.`def`.map(_.text))
      val diff = words diff checked
      translations ++= tr
      context.become(run(diff))
  }

  def run(diff: Set[String]): Receive = {
    def sendReqAndSaveRef(tx: String) = {
      log.info(s"Translate $tx")
      val senderRef = context.actorOf(Sender.props(TranslationRequest(lg, tx)))
      senders += senderRef
    }

    if (diff.isEmpty) context.parent ! Translations(nonEmpty)
    else diff.foreach(sendReqAndSaveRef)
    receiving()
  }

  def receiving(): Receive = {
    case tr: TranslatedWord if tr.`def`.nonEmpty =>
      translations += tr
      cache ! tr
      log.info(s"Add to cache")
    case ans @ Answer(_, _, _) => log.error("Error ", ans.code, ans.msg)
    case Sender.Done =>
      log.info("Done")
      senders -= sender
      println("size" + senders.size)

      if (senders.isEmpty) {
        context.parent ! Translations(nonEmpty)
      }
    case _ => log.info("Empty translation")
  }

  def nonEmpty: Set[TranslatedWord] = translations.filter(_.`def`.nonEmpty)
}

object Controller {

  case class Check(words: Set[String])

  case class Checked(tr: Set[TranslatedWord])

  def props(cache: ActorRef)(implicit mat: ActorMaterializer) = Props(new Controller(cache))
}


