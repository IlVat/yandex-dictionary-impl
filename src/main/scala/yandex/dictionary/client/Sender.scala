package yandex.dictionary.client

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success, Try}

import akka.Done
import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.settings.ConnectionPoolSettings
import akka.http.scaladsl.Http.HostConnectionPool
import akka.http.scaladsl.{Http, HttpExt}
import akka.http.scaladsl.model._
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, OverflowStrategy, QueueOfferResult}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.util.ByteString

import pushka.json._
import yandex.dictionary.DictionarySettings
import yandex.dictionary.DictionarySettings._
import yandex.dictionary.model.{TranslatedWord, TranslationRequest}

class Sender(request: TranslationRequest)(
  implicit materializer: ActorMaterializer) extends Actor with ActorLogging {
  
  import akka.pattern.pipe
  
  implicit val system = context.system
  
  val uri = dictUri + s"key=$dictKey&lang=${ request.lang }&text=${ request.text }"
  val http = Http(context.system)
  val pool = http.cachedHostConnectionPoolHttps[Promise[HttpResponse]](dictHost
    , settings = ConnectionPoolSettings(DictionarySettings.rootConfig))
  
   

  val queue = Source.queue[(HttpRequest, Promise[HttpResponse])](100, OverflowStrategy.backpressure)
    .via(pool)
    .toMat(Sink.foreach({
      case ((Success(resp), p)) => p.success(resp)
      case ((Failure(e), p)) =>
        println("what a fuck " + e.getMessage)
        p.failure(e)
    }))(Keep.left)
    .run

  def queueRequest(request: HttpRequest): Future[HttpResponse] = {
    val responsePromise = Promise[HttpResponse]()
    queue.offer(request -> responsePromise).flatMap {
      case QueueOfferResult.Enqueued    => responsePromise.future
      case QueueOfferResult.Dropped     =>
        Future.failed(new RuntimeException("Queue overflowed. Try again later."))
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.QueueClosed =>
        Future
          .failed(new RuntimeException("Queue was closed (pool shut down) while running the request. Try again later."))
    }
  }
  
  override def preStart(): Unit = queueRequest(HttpRequest(uri = uri)).pipeTo(self)
  
  
  override def receive: Receive = {
    case resp @ HttpResponse(StatusCodes.OK, headers, entity, _) =>
      val parent = context.parent
      val selfRef = self
      
      fold(entity) foreach { body =>
        log.info("Got success response.")
        parent ! read[TranslatedWord](body)
        parent ! Sender.Done
        context.stop(selfRef)
      }
    case resp @ HttpResponse(code, _, _, _) =>
      log.info("Request failed, response code: " + code)
      resp.discardEntityBytes()
      stop()
    case x =>
      log.error(s"$x problem")
      stop()
  }
  
  private def fold(entity: ResponseEntity): Future[String] = {
    entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.decodeString("utf-8"))
  }
  
  def stop(): Unit = {
    context.parent ! Sender.Done
    context.stop(self)
  }
}

object Sender {
  case object Done
  case object Abort
  
  def props(request: TranslationRequest)(
    implicit mat: ActorMaterializer) = Props(new Sender(request)(mat))
}
