package yandex.dictionary

import java.net.InetAddress

import scala.util.Try

import com.typesafe.config.ConfigFactory


object DictionarySettings {
  
  val localAddress = InetAddress.getLocalHost.getHostAddress

  val rootConfig =  ConfigFactory.load()

  protected val common = rootConfig.getConfig("dictionary")
  val interface = withFallback[String](Try(common.getString("interface")), "interface") getOrElse localAddress
  val port = withFallback[Int](Try(common.getInt("port")), "port") getOrElse 80
  val sys = common.getString("system")

  //Yandex API keys
  val dictKey = common.getString("dictKey")
  val trKey = common.getString("trKey")
  val dictUri = common.getString("dictUri")
  val dictHost = common.getString("dictHost")
  val timeout = rootConfig.getDuration("akka.http.server.request-timeout")

  def withFallback[T](env: Try[T], key:String): Option[T] = env match {
    case null => None
    case value => value.toOption
  }
}
