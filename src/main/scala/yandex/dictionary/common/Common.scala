package yandex.dictionary.common

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

trait Common {
  implicit val system: ActorSystem
  implicit  val materializer: ActorMaterializer
}
