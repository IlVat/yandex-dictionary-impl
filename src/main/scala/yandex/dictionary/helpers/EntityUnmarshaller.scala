package yandex.dictionary.helpers

import scala.concurrent.{ExecutionContext, Future}

import akka.http.scaladsl.model.{ContentTypeRange, ContentTypes, HttpEntity}
import akka.http.scaladsl.unmarshalling.Unmarshaller._
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import akka.stream.Materializer
import akka.util.ByteString

import pushka.Reader
import pushka.json._

object EntityUnmarshaller {
  val supported: Set[ContentTypeRange] = Set[ContentTypeRange](ContentTypes.`application/json`)

  def create[T: Reader]: FromEntityUnmarshaller[T] = {
    new Unmarshaller[HttpEntity, T] {
      def apply(entity: HttpEntity)(implicit ec: ExecutionContext,
          materializer: Materializer): Future[T] = {
        val future = entity.contentType match {
          case ContentTypes.`application/json` => Future.successful(entity)
          case other => Future.failed(new UnsupportedContentTypeException(supported))
        }
        future.flatMap(_.dataBytes.runFold(ByteString.empty)(_ ++ _)
          .map(str => read[T](str.utf8String)))(ec)
      }
    } forContentTypes(supported.toList: _*)
  }
}
