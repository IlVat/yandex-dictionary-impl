package yandex.dictionary.helpers

object TextHandler {
  def handle(text: String): Set[String] = {
    text.toLowerCase.replaceAll("""[^\p{Ll}\p{Lt} ]""", "").split(" ").toSet.filter(_ != "")
  }
}
