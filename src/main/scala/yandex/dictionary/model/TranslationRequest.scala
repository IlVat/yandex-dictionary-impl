package yandex.dictionary.model

import pushka.annotation.pushka


@pushka
case class TranslationRequest(lang: String = "", text: String = "")


