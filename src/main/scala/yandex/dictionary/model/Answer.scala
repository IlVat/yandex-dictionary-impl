package yandex.dictionary.model

import pushka.annotation.pushka

@pushka
final case class Answer[T](
    code: Int,
    msg: String,
    entity: Option[T] = None)

object Answers {
  val Ok = Answer[String](200, "Operation completed successfully.")
  val BadRequest = Answer[String](400, "Bad request.")
  val Forbidden = Answer[String](403, "Forbidden")

  def Ok[T](data: T): Answer[T] = Answer[T](Ok.code, Ok.msg, Option(data))

  def answered(code: Int): Answer[String] = code match {
    case 200 => Ok
    case 400 => BadRequest
    case 403 => Forbidden
    case _ => Answer(404, "Undefined error.")
  }
}



