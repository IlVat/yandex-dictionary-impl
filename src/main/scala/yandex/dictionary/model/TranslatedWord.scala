package yandex.dictionary.model

import pushka.annotation.{forceObject, pushka}

@pushka
@forceObject
case class Translations(trs: Set[TranslatedWord])

/**
  *
  * @param `def` - Set of dictionary entries.
  */
@pushka
@forceObject
final case class TranslatedWord(`def`: Set[TranslatedPair])


/**
  *
  * @param text - Text of the entry.
  * @param pos  - Part of speech (may be omitted).
  * @param ts   - A transcription of the search word.
  * @param tr   - Set of translations.
  */
@pushka
final case class TranslatedPair(
    text: String,
    pos: Option[String] = None,
    ts: Option[String] = None,
    tr: Set[Translation])


/**
  *
  * @param text - Text of the entry.
  * @param pos  - Part of speech (may be omitted).
  * @param syn  - Seq of synonyms (may be omitted).
  * @param mean - Seq of meanings (may be omitted).
  * @param ex   - Seq of examples (may be omitted).
  */
@pushka
final case class Translation(
    text: String,
    pos: Option[String] = None,
    syn: Option[Seq[Synonym]] = None,
    mean: Option[Seq[SingleText]] = None,
    ex: Option[Seq[Example]] = None)


/**
  *
  * @param text - Text of the entry.
  * @param pos  - Part of speech (may be omitted).
  * @param gen - Gender (may be omitted).
  */
@pushka
final case class Synonym(
    text: String,
    pos: Option[String] = None,
    gen: Option[String] = None)


/**
  *
  * @param text - Text of the entry.
  * @param tr - Seq of translations
  */
@pushka
final case class Example(text: String, tr: Seq[SingleText])

@pushka
@forceObject
final case class SingleText(text: String)

