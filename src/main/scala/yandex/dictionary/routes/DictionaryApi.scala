package yandex.dictionary.routes

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout

import yandex.dictionary.model.{TranslationRequest, Translations}

trait DictionaryApi {

  def createTranslator(): ActorRef

  implicit def executionContext: ExecutionContext
  implicit def requestTimeout: Timeout

  lazy val translator = createTranslator()

  def translate(req: TranslationRequest): Future[Translations] = translator.ask(req).mapTo[Translations]
}
