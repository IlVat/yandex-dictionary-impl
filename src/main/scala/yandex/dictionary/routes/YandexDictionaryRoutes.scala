package yandex.dictionary.routes

import scala.concurrent.ExecutionContext

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import akka.stream.ActorMaterializer
import akka.util.Timeout

import pushka.json._
import yandex.dictionary.core.Receptionist
import yandex.dictionary.helpers.EntityUnmarshaller
import yandex.dictionary.model._

class YandexDictionaryApi(system: ActorSystem, timeout: Timeout) extends YandexDictionaryRoutes {
  override implicit val requestTimeout: Timeout = timeout
  override implicit val executionContext: ExecutionContext = system.dispatcher

  override def createTranslator(): ActorRef = system.actorOf(Props[Receptionist], "receptionist")
}


trait YandexDictionaryRoutes extends DictionaryApi {
  implicit val um: FromEntityUnmarshaller[TranslationRequest] = EntityUnmarshaller.create[TranslationRequest]

  val route = path("text") {
    post {
      entity(as[TranslationRequest]) { text =>
        onSuccess(translate(text)) {
          case tr: Translations => complete(write(Answers.Ok(tr)))
          case _ => complete(write(Answers.Forbidden))
        }
      }
    }
  }

  //TODO load text from url
  //TODO load text from file
}
